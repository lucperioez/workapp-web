
const onChange = function(image) {
    const vm = this; 
    if (image) {
        vm.image_info = image
    } else {
    console.log('FileReader API not supported: use the <form>, Luke!')
    }
}

const getImages = function(){
    const vm = this;
    vm.images_url = vm.$apiUrl;
    axios
        .get(vm.$apiUrl + 'adwords/')
        .then (response => {
            vm.images = response.data;
        })
}

const deleteImage = function(id_image){
    const vm = this;
    axios
        .delete(vm.$apiUrl + 'adwords/' + id_image)
        .then(response => {
            vm.getImages();
        })
}

const addImage = function (){
    const vm = this;
    axios
        .post(vm.$apiUrl + 'adwords/',{
            imagen:vm.image_info,
            url:vm.url
        })
        .then(response => {
            vm.$router.go(0);   
        })
}

export const advertisingComponent = {
    data () {
        return {
            url:'',
            image_info:'',
            images:[],
            images_url:''
        }
    },
    components: {
        PictureInput
    },
    mounted:getImages,
    methods: {
        onChange:onChange,
        getImages:getImages,
        deleteImage: deleteImage,
        addImage: addImage
    },

    template: /*html*/` 
    <section>
        <div class = "row">
            <div class ="col-sm-3">
                <picture-input 
                ref="pictureInput"
                width="200" 
                height="90" 
                accept="image/jpeg,image/png" 
                size="20" 
                button-class="btn"
                :custom-strings="{
                    upload: '<h1>Bummer!</h1>',
                    drag: 'Drag an image'
                }"
                @change="onChange">
                </picture-input>
            </div>
            <div class = "col-sm-3">
                <input type = "text" v-model="url" class="form-control m-input m-input--pill" placeholder="URL">
            </div>
            <div class = "col-sm-3">
                <button class="btn btn-primary btn-sm" @click="addImage();">Add Image</button>
            </div>
        </div>
        <div class ="row">
            <div class ="col-sm-3" v-for = "image in images">
                <img v-bind:src ="images_url + image.url" width ="200" height="100">
                <button  style="margin:auto;display:block;" class="btn btn-primary btn-sm" @click="deleteImage(image.id_adwords)">Delete</button>
            </div>
        </div>
    </section>`
};