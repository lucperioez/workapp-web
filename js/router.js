import {employeesComponent} from '../js/employees/employees.js';
import {buildersComponent} from '../js/builders/builders.js';
import {advertisingComponent} from '../js/advertising/advertising.js';
import {provincesComponent} from '../js/provinces/provinces.js';

export const router = new VueRouter({
    // mode: 'history',
    linkActiveClass: 'active',
    routes:[
        {
            path: '/employees',
            component: employeesComponent
        },
        {
            path: '/builders',
            component: buildersComponent
        },
        {
            path: '/',
            component: buildersComponent
        },
        {
            path: '/advertising',
            component: advertisingComponent
        },
        {
            path: '/provinces',
            component: provincesComponent
        },
    ]
});