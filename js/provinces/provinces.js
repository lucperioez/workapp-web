const getProvinces = function(){
    const vm = this;
        axios
            .get(vm.$apiUrl + 'province/')
            .then(response => {
                vm.provincesData = response.data;
            })
        vm.userData = JSON.parse(localStorage.getItem('userData')); 
    
}

const addProvince = function(){
    const vm = this;
        axios
            .post(vm.$apiUrl + 'province/',{
                name:vm.province
            })
            .then(response =>{
                vm.province = '';
                vm.getProvinces();
            })
}

const deleteProvince = function(id_province){
    const vm = this;
        axios
            .delete(vm.$apiUrl + 'province/' + id_province)
            .then(response =>{
                $('#deleteProvince').modal('hide');
                vm.getProvinces();
            })
}

const editProvinceModal = function(id_province,province){
    const vm = this;
    vm.province = province;
    vm.id_province = id_province;
    $('#province').modal('show');
}

const getCities = function(){
    const vm = this;
        axios
            .get(vm.$apiUrl + 'province/city/' + vm.selected_province)
            .then(response => {
                vm.citiesData = response.data;
            })
        vm.userData = JSON.parse(localStorage.getItem('userData')); 
}

const addCity = function(){
    const vm = this;
        axios
            .post(vm.$apiUrl + 'province/city',{
                name:vm.city,
                province_id:vm.selected_province
            })
            .then(response =>{
                vm.getCities();
                vm.city='';
            })
}

const deleteCity = function(id_city){
    const vm = this;
        axios
            .delete(vm.$apiUrl + 'province/city/' + id_city)
            .then(response =>{
                $('#deleteCity').modal('hide');
                vm.getCities();
            })
}

const updateProvince = function(id_province){
    const vm = this;
        axios
            .put(vm.$apiUrl + 'province/' + id_province,{
                name:vm.province
            })
            .then(response => {
                $('#province').modal('hide');
                vm.province='';
                vm.getProvinces();
            })
}

const editCityModal = function(id_city,city){
    const vm = this;
    vm.city = city;
    vm.id_city = id_city;
    $('#city').modal('show');
}

const updateCity = function(id_city){
    const vm = this;
        axios
            .put(vm.$apiUrl + 'province/city/' + id_city ,{
                name:vm.city
            })
            .then(response=>{
                $('#city').modal('hide');
                vm.city='';
                vm.getCities();
            })  
}

const closeModalCity = function(){
    const vm = this;
    $('#city').modal('hide');
    vm.city='';
}

const closeModalProvince = function(){
    const vm = this;
    $('#province').modal('hide');
    vm.province='';
}

const deleteModal = function(id_city){
    const vm = this;
    $('#deleteCity').modal('show');
    vm.id_city = id_city;
}

const deleteModalProvince = function (id_province){
    const vm = this;
    $('#deleteProvince').modal('show');
    vm.id_province = id_province;
}
export const provincesComponent = {

    data: function(){
        return {
            provincesData:[],
            province:'',
            selected_province:'',
            citiesData:[],
            city:'',
            id_city:'',
            id_province:''
        }
    },

    mounted: getProvinces,
    methods: {
                getProvinces:getProvinces,
                addProvince:addProvince,
                getCities:getCities,
                addCity:addCity,
                deleteCity:deleteCity,
                editCityModal:editCityModal,
                updateCity:updateCity,
                deleteProvince:deleteProvince,
                editProvinceModal:editProvinceModal,
                updateProvince:updateProvince,
                closeModalCity:closeModalCity,
                closeModalProvince:closeModalProvince,
                deleteModal:deleteModal,
                deleteModalProvince:deleteModalProvince
            },

    template: /*html*/` 
    <section>
        <div class="m-content">
            <div class = "row">
                <div class = "col-xl-6">
                    <div class = "row">
                        <div class = "col-sm-6">
                            <input type = "text" v-model="province" class="form-control m-input m-input--pill" placeholder="Province...">
                        </div>
                        <div class = "col-sm-6">
                            <button type="button" class="btn m-btn--pill m-btn--air btn-primary btn-sm" @click="addProvince();">Add Province</button>
                        </div>
                    </div> 
                </div>
                <div class = "col-xl-6">
                    <div class = "row">
                        <div class = "col-sm-4">
                            <select name = "provinces" class="form-control m-input m-input--square" v-model="selected_province" @change="getCities();">
                                <option v-for="prov in provincesData" v-bind:value="prov.id_province">{{prov.name}}</option>
                            </select>
                        </div>
                        <div class = "col-sm-4">
                            <input type = "text" v-model="city" class="form-control m-input m-input--pill" placeholder="City...">
                        </div>
                        <div class = "col-sm-4">
                            <button type="button" class="btn m-btn--pill m-btn--air btn-primary btn-sm" @click="addCity();">Add City</button>
                        </div>
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-xl-6">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Provinces Table
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">

                            <!--begin::Section-->
                            <div class="m-section">
                                <div class="m-section__content">
                                    <table class="table m-table m-table--head-bg-primary">
                                        <thead>
                                            <tr>
                                                <th>ID Province</th>
                                                <th>Province</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for = "province in provincesData">
                                                <td>{{province.id_province}}</td>
                                                <td>{{province.name}}</td>
                                                <td>
                                                    <i class="fa fa-edit" @click="editProvinceModal(province.id_province,province.name)"></i>
                                                    <i class="fa fa-trash-alt" @click="deleteModalProvince(province.id_province)"></i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">

                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Table Of Cities By Province
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-section">
                                <div class="m-section__content">
                                    <table class="table m-table m-table--head-bg-primary">
                                        <thead>
                                            <tr>
                                                <th>ID City</th>
                                                <th>City</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="city in citiesData">
                                                <td>{{city.id_city}}</td>
                                                <td>{{city.name}}</td>
                                                <td>
                                                    <i class="fa fa-edit" @click="editCityModal(city.id_city,city.name)"></i>
                                                    <i class="fa fa-trash-alt" @click="deleteModal(city.id_city)"></i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade show" id="city" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit City</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Selected City:</label>
                            <input type="text" class="form-control" v-model="city" id="recipient-name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" @click="closeModalCity();">Cancel</button>
                        <button type="button" class="btn btn-primary" @click="updateCity(id_city)">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade show" id="province" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Province</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Selected City:</label>
                            <input type="text" class="form-control" v-model="province" id="recipient-name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" @click="closeModalProvince();">Cancel</button>
                        <button type="button" class="btn btn-primary" @click="updateProvince(id_province)">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade show" id="deleteCity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this City?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" @click="deleteCity(id_city)">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade show" id="deleteProvince" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this Province?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" @click="deleteProvince(id_province)">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
     `
};