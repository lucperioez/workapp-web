import {router} from '../js/router.js';

(function(){
    
    'use strict';
    
    Vue.prototype.$apiUrl = 'http://138.68.56.247/workapp-backend/'

    const work = new Vue({
        router,
        el: '#workapp',
        components: {
        'picture-input': PictureInput
      }});
    
 })();