
(function(){
    
    'use strict';
    
    Vue.prototype.$apiUrl = 'http://138.68.56.247/workapp-backend/'

    const work = new Vue({
        el: '#login',
        data: function(){
            return {
               email: "",
               password: "",
            }
        },
        methods:{
            login(){
                const vm = this;
                axios
                    .post(vm.$apiUrl + 'users/login',{
                        email:vm.email,
                        password:vm.password
                    })
                    .then(response => {
                        const userData = response.data;
                        if(userData.user.status == 1 && userData.user.role_id == 1){
                            localStorage.setItem('userData',JSON.stringify(userData));
                            window.location.href="index.html";
                        }else{
                            alert("No tiene Permisos para ingresar");
                        }
                    })
            }
        }
    });
    
 })();