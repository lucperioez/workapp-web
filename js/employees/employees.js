const getEmployees = function(){
    const vm = this;
    axios
        .get(vm.$apiUrl + 'users/rol/' + 3)
        .then(response => {
            vm.employeeData = response.data.users;
        })
    vm.userData = JSON.parse(localStorage.getItem('userData')); 
    
}

const modalDetail = function (employee){
    const vm = this;
    $('#modalDetail').modal('show');
    vm.employeeDetail = employee.detail;
    vm.employeeDetail.email = employee.user;
}

export const employeesComponent = {

    data: function(){
        return {
            employeeData:[],
            employeeDetail:{}
        }
    },

    mounted: getEmployees,
    methods: {
        getEmployees:getEmployees,
        modalDetail:modalDetail
    },

    template: /*html*/` 
    <section>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                List of Employees
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div id="m_table_1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline collapsed" id="m_table_1" role="grid" aria-describedby="m_table_1_info" style="width: 974px;">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 57.25px;" aria-label="Country: activate to sort column ascending">User ID</th>
                                            <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 105.25px;" aria-label="CompanyAgent: activate to sort column ascending">Profile Picture</th>
                                            <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 65.25px;" aria-label="ShipCity: activate to sort column ascending">Email</th>
                                            <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 92.25px;" aria-label="ShipAddress: activate to sort column ascending">Date Of Signup</th>
                                            <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 105.25px;" aria-label="CompanyAgent: activate to sort column ascending">Status</th>
                                            <th class="sorting" tabindex="0" aria-controls="m_table_1" rowspan="1" colspan="1" style="width: 105.25px;" aria-label="CompanyAgent: activate to sort column ascending">Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr role="row" class="odd" v-for="employee in employeeData">
                                            <td class="sorting_1">{{employee.id_user}}</td>
                                            <td><img v-bind:src ="employee.detail.image" width ="50" height="50"></td>                                            
                                            <td>{{employee.user}}</td>
                                            <td>{{employee.created_date}}</td>
                                            <td v-if="employee.status == 1"><span class="m-badge  m-badge--success m-badge--wide">Active</span></td>
                                            <td v-if="employee.status == 0"><span class="m-badge  m-badge--danger m-badge--wide">Inactive</span></td>
                                            <td v-if="employee.detail != false"><i class="fa fa-list-alt" @click="modalDetail(employee)"></i></td>
                                            <td v-else>No More Data Available</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="dataTables_info" id="m_table_1_info" role="status" aria-live="polite">Showing 1 to 10 of 50 entries</div>
                        </div>
                        <div class="col-sm-12 col-md-7 dataTables_pager">
                            <div class="dataTables_length" id="m_table_1_length">
                                <label>Display <select name="m_table_1_length" aria-controls="m_table_1" class="form-control form-control-sm">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                </select></label>
                            </div>
                            <div class="dataTables_paginate paging_simple_numbers" id="m_table_1_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button page-item previous disabled" id="m_table_1_previous">
                                        <a href="#" aria-controls="m_table_1" data-dt-idx="0" tabindex="0" class="page-link">
                                            <i class="la la-angle-left"></i>
                                        </a>
                                    </li>
                                    <li class="paginate_button page-item active">
                                        <a href="#" aria-controls="m_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a>
                                    </li>
                                    <li class="paginate_button page-item ">
                                        <a href="#" aria-controls="m_table_1" data-dt-idx="2" tabindex="0" class="page-link">2</a>
                                    </li>
                                    <li class="paginate_button page-item ">
                                        <a href="#" aria-controls="m_table_1" data-dt-idx="3" tabindex="0" class="page-link">3</a>
                                    </li>
                                    <li class="paginate_button page-item ">
                                        <a href="#" aria-controls="m_table_1" data-dt-idx="4" tabindex="0" class="page-link">4</a>
                                    </li>
                                    <li class="paginate_button page-item ">
                                        <a href="#" aria-controls="m_table_1" data-dt-idx="5" tabindex="0" class="page-link">5</a>
                                    </li>
                                    <li class="paginate_button page-item next" id="m_table_1_next">
                                        <a href="#" aria-controls="m_table_1" data-dt-idx="6" tabindex="0" class="page-link">
                                            <i class="la la-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalDetail"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
              <div class="modal-content">
                 <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                 </div>
                    <label class="form-control">First Name: {{employeeDetail.first_name}} </label>
                    <label class="form-control">Last Name: {{employeeDetail.last_name}} </label>
                    <label class="form-control">Work Type:  {{employeeDetail.work_type}}</label>
                    <label class="form-control">Email:  {{employeeDetail.email}}</label>
                    <label class="form-control">Phone:  {{employeeDetail.phone}}</label>
                    <label class="form-control">Cities:  {{employeeDetail.city}}</label>
                    <label class="form-control">Personal Description: {{employeeDetail.description}} </label>
                    <label class="form-control">Laboral Experience:  {{employeeDetail.experience}}</label>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
              </div>
           </div>
        </div>
    </section>
     `
};